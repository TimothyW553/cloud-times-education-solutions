import java.util.*;
import java.io.*;

public class Main {

    static int[][] dist;
    static int N, M;
    static final int INF = 0x3f3f3f3f;

    static boolean floydwarshall() {
        for(int k = 1; k <= N; k++) {
            for(int u = 1; u <= N; u++) {
                for(int v = 1; v <= N; v++) {
                    if(dist[u][k] != INF && dist[k][v] != INF) {
                        dist[u][v] = Math.min(dist[u][v], dist[u][k] + dist[k][v]);
                    }
                }
            }
        }
        for(int u = 1; u <= N; u++) {
            for(int v = 1; v <= N; v++) {
                if(dist[u][v] < 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();
        
        dist = new int[N + 5][N + 5];
        for(int i = 0; i < N + 5; i++) {
            Arrays.fill(dist[i], INF);
        }
        for(int i = 1; i <= N; i++) {
            dist[i][i] = 0;
        }
        for(int i = 0; i < M; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();
            dist[a][b] = c;
        }
        boolean good = floydwarshall();
        if(!good) {
            System.out.println("Negative cycle detected");
        } else {
            System.out.println(dist[1][N]);
        }
    }
}
