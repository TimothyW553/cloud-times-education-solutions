import java.util.*;
import java.io.*;

public class Main {

    static class Pair {
        int nd, w;

        public Pair(int nd, int w) {
            this.nd = nd;
            this.w = w;
        }
    }

    static ArrayList<Pair>[] adj;
    static int[] dist;
    static int[] count;
    static boolean[] inQueue;
    static int N, M;
    static final int INF = 0x3f3f3f3f;

    static int bellmanFord(int s, int t) {
        Arrays.fill(dist, INF);
        dist[s] = 0;
        for (int i = 1; i <= N; i++) {
            for (int u = 1; u <= N; u++) {
                for (Pair e : adj[u]) {
                    int v = e.nd;
                    int w = e.w;
                    if (dist[u] != INF && dist[u] + w < dist[v]) {
                        if (i == N) {
                            return -1;
                        } else {
                            dist[v] = dist[u] + w;
                        }
                    }
                }
            }
        }
        return dist[t];
    }

    static int SPFA(int s, int t) {
        Arrays.fill(dist, INF);
        dist[s] = 0;
        count[s] = 1;
        Queue<Integer> q = new LinkedList<>();
        q.add(s);
        inQueue[s] = true;
        while (!q.isEmpty()) {
            int cur = q.poll();
            inQueue[cur] = false;
            for (Pair e : adj[cur]) {
                int v = e.nd;
                int w = e.w;
                if (dist[cur] + w < dist[v]) {
                    cnt[v]++;
                    if(cnt[v] >= N) {
                        return -1;
                    }
                    dist[v] = dist[cur] + w;
                    if (!inQueue[v]) {
                        inQueue[v] = true;
                        q.add(v);
                    }
                }
            }
        }
        return dist[t];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();

        adj = new ArrayList[N + 5];
        dist = new int[N + 5];
        inQueue = new boolean[N + 5];
        count = new int[N + 5];
        for (int i = 0; i < N + 5; i++) {
            adj[i] = new ArrayList<>();
        }

        for (int i = 0; i < M; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();
            adj[a].add(new Pair(b, c));
        }

        int dist_ = SPFA(1, N);
        if (dist_ == INF) {
            System.out.println("No path exists from S to T");
        } else if (dist_ == -1) {
            System.out.println("Negative weight cycle detected");
        } else {
            System.out.println(dist_);
        }
    }
}
