import java.util.*;
import java.io.*;

public class Main {

	static class Pair {
		int n1;
		double w;
		public Pair(int n1, double w) {
			this.n1 = n1;
			this.w = w;
		}
	}
	
	static Map<String, Integer> names;
	static ArrayList<Pair>[] adj;
	static double[] dist;
	static double eps = 1e-6;

	public static void main(String[] args) {
		int N = readInt();
		int M = readInt();
		
		names = new HashMap<>();
		adj = new ArrayList[N+5];
		dist = new double[N+5]; // leave dist array as filled with 0
		for(int i = 0; i < N+5; i++) {
			adj[i] = new ArrayList<>();
		}

		for(int i = 0; i < N; i++) {
			String s = read();
			names.put(s, i);
		}

		for(int i = 0; i < M; i++) {
			String a = read();
			String b = read();
			double w = readDouble();
			int u = names.get(a);
			int v = names.get(b);
			adj[u].add(new Pair(v, w));
		}
		
		int ap = names.get("APPLES");
		dist[ap] = 1.0;
		for(int i = 1; i <= N; i++) { // number of iterations -- we need to relax N-1 times
		                              // but if we relax the edges N times, theres cycle
			for(int u = 0; u < N; u++) { // go through each node
				for(Pair e : adj[u]) { // go through all the edges of that node
					int v = e.n1;
					double w = e.w;
					if(dist[u] * w > dist[v]) { // if the amount exchange fruit v for is better than the one that already exists
						if(i == N && v == ap) {
							// if we can relax the number of apples still and we're on
							// the Nth iteration, then theres a infinite cycle
							System.out.println("YA");
							return; // stops program
						}
						dist[v] = dist[u] * w; // then let's update it
					}
				}
			}
		}
		System.out.println("NAW");
	}

	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer st;

	static String read() {
		while (st == null || !st.hasMoreTokens()) {
			try { st = new StringTokenizer(br.readLine()); 
			} catch (IOException e) {}
		}
		return st.nextToken();
	}
	static int readInt() {
		return Integer.parseInt(read());
	}
	static long readLong() {
		return Long.parseLong(read());
	}
	static double readDouble() {
		return Double.parseDouble(read());
	}
}
