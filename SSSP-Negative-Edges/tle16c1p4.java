import java.util.*;
import java.io.*;

public class Main {

	static int[] microCnt;
	static int[][] dist;
	static boolean[][] use;

	public static void main(String[] args) {
		int N = readInt();
		int M = readInt();
		int K = readInt();
		
		microCnt = new int[N+5];
		dist = new int[N+5][N+5];
		use = new boolean[N+5][N+5];
		for(int i = 0; i < N+5; i++) {
			Arrays.fill(dist[i], 0x3f3f3f3f);
		}
		
		for(int i = 1; i <= N; i++) {
			microCnt[i] = readInt();
			dist[i][i] = 0;
		}
		
		for(int i = 0; i < M; i++) {
			int u = readInt();
			int v = readInt();
			int d = readInt();
			dist[u][v] = dist[v][u] = d;
		}
		
		for(int k = 1; k <= N; k++) { // run floyd warshall all pairs shortst path
			for(int i = 1; i <= N; i++) {
				for(int j = 1; j <= N; j++) {
					dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
				}
			}
		}

		for(int i = 1; i <= N; i++) { // boolean matrix to determine whether the dist between rooms i and j is <= K
			for(int j = 1; j <= N; j++) {
				if(dist[i][j] <= K) {
					use[i][j] = true; // set the jth room to true, if it is visitable from room i
				}
			}
		}
		
		int ans = 0; // max # of microwaves that we destroy
		for(int i = 1; i <= N; i++) {
			for(int j = i + 1; j <= N; j++) {
				for(int k = j + 1; k <= N; k++) {
					boolean[] vis = new boolean[N+5]; // vis array to keep track of all rooms visitable from {i, j, k}
					int sum = 0;
					for(int t = 1; t <= N; t++) {
						vis[t] = (use[i][t] | use[j][t] | use[k][t]);
						if(vis[t]) {
							sum += microCnt[t];
						}
					}	
					ans = Math.max(ans, sum);
				}
			}
		}
		System.out.println(ans);
	}
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer st;

	static String read() {
		while (st == null || !st.hasMoreTokens()) {
			try { st = new StringTokenizer(br.readLine()); 
			} catch (IOException e) {}
		}
		return st.nextToken();
	}
	static int readInt() {
		return Integer.parseInt(read());
	}
	static long readLong() {
		return Long.parseLong(read());
	}
	static double readDouble() {
		return Double.parseDouble(read());
	}
}
