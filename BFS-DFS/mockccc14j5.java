import java.util.*;

public class mockccc14j5 {

    static class Pair {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
    }

    static class State {
        int t, x, y;
        public State(int t0, int x0, int y0) {
            t = t0; x = x0; y = y0;
        }
    }

    static char[][][] mat;
    static int [][][] dist;
    static Pair[] loc = {new Pair(1, 0), new Pair(-1, 0), new Pair(0, 1), new Pair(0, -1)};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt();
        int C = sc.nextInt();
        int T = sc.nextInt();
        mat = new char[T+5][R+5][C+5];
        dist = new int[T+5][R+5][C+5];
        Pair a = new Pair(0, 0);
        Pair b = new Pair(0, 0);
        for(int t = 0; t < T; t++) {
            for(int i = 0; i < R; i++) {
                String line = sc.next();
                for(int j = 0; j < C; j++) {
                    dist[t][i][j] = -1; // -1 meaning unvisited
                    mat[t][i][j] = line.charAt(j);
                    if(mat[t][i][j] == 'A') {
                        a.f = i;
                        a.s = j;
                    }
                    if(mat[t][i][j] == 'B') {
                        b.f = i;
                        b.s = j;
                    }
                }
            }
        }
        Queue<State> q = new LinkedList<>();
        dist[0][a.f][a.s] = 0;
        q.add(new State(0, a.f, a.s));
        while(!q.isEmpty()) {
            State cur = q.poll();
            for(Pair dir : loc) {
                int dim = cur.t;
                int r = cur.x + dir.f;
                int c = cur.y + dir.s;
                if(r >= 0 && r < R && c >= 0 && c < C && dist[dim][r][c] == -1 && mat[dim][r][c] != 'X') {
                    dist[dim][r][c] = dist[dim][cur.x][cur.y] + 1;
                    q.add(new State(dim, r, c));
                }
            }
            for(int i = 0; i < T; i++) {
                if(cur.t == i) {
                    continue;
                }
                int dim = cur.t;
                int r = cur.x;
                int c = cur.y;
                if(dist[i][r][c] == -1 && mat[i][r][c] != 'X') {
                    dist[i][r][c] = dist[dim][r][c] + 1;
                    q.add(new State(i, r, c));
                }
            }
        }
        System.out.println(dist[0][b.f][b.s]);
    }
}
