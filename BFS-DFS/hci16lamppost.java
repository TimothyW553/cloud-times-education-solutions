import java.util.*;

public class hci16lamppost {
    
    static int[] deg;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        deg = new int[N+5];
        int C = sc.nextInt();
        for(int i = 0; i < C; i++) {
            int C1 = sc.nextInt();
            int C2 = sc.nextInt();
            deg[C1]++;
            deg[C2]++;
        }
        int idx = 0; // idx of highest deg
        int max = 0; // val of highest deg
        for(int i = 0; i <= N; i++) {
            if(deg[i] >= max) {
                max = deg[i];
                idx = i;
            }
        }
        System.out.println(idx);
    }
}
