import java.util.*;

public class ccc08s3 {
    
    static class Pair {
        int r, c;
        public Pair(int r0, int c0) {
            r = r0;
            c = c0;
        }
    }
    
    static int aDirs[][] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    static int hDirs[][] = {{0, -1}, {0, 1}};
    static int vDirs[][] = {{-1, 0}, {1, 0}};
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        while(T-- > 0) {
            int R = sc.nextInt();
            int C = sc.nextInt();
            char grid[][] = new char[R+5][C+5];
            int dist[][] = new int[R+5][C+5];
            for(int i = 0; i < R; i++) {
                String row = sc.next();
                for(int j = 0; j < C; j++) {
                    grid[i][j] = row.charAt(j);
                    dist[i][j] = -1;
                }
            }
            Queue<Pair> q = new LinkedList<>();
            q.add(new Pair(0, 0));
            dist[0][0] = 1;
            while(!q.isEmpty()) {
                Pair cur = q.poll();
                int r0 = cur.r;
                int c0 = cur.c;
                if(grid[r0][c0] == '+') {
                    for(int[] x : aDirs) {
                        int r = r0 + x[0];
                        int c = c0 + x[1];
                        if(r >= 0 && r < R && c >= 0 && c < C && dist[r][c] == -1 && grid[r][c] != '*') {
                            dist[r][c] = dist[r0][c0] + 1;
                            q.add(new Pair(r, c));
                        }
                    }
                } else if(grid[r0][c0] == '-') {
                    for(int[] x : hDirs) {
                        int r = r0 + x[0];
                        int c = c0 + x[1];
                        if(r >= 0 && r < R && c >= 0 && c < C && dist[r][c] == -1 && grid[r][c] != '*') {
                            dist[r][c] = dist[r0][c0] + 1;
                            q.add(new Pair(r, c));
                        }
                    }
                } else if(grid[r0][c0] == '|') {
                    for(int[] x : vDirs) {
                        int r = r0 + x[0];
                        int c = c0 + x[1];
                        if(r >= 0 && r < R && c >= 0 && c < C && dist[r][c] == -1 && grid[r][c] != '*') {
                            dist[r][c] = dist[r0][c0] + 1;
                            q.add(new Pair(r, c));
                        }
                    }
                }
            }
            System.out.println(dist[R-1][C-1]);
        }
    }
}
