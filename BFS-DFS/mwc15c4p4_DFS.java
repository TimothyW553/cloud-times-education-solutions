import java.util.*;

public class mwc15c4p4_DFS {
    
    static ArrayList<Integer> adj[];
    static boolean[] vis;
    
    static void DFS(int n) {
        vis[n] = true;
        for(int v : adj[n]) {
            if(!vis[v]) {
                DFS(v);
            }
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        adj = new ArrayList[N+5];
        vis = new boolean[N+5];
        for(int i = 0; i < N+5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < N; i++) {
            int X = sc.nextInt();
            int Y = sc.nextInt();
            adj[X].add(Y);
        }
        int A = sc.nextInt(); // start
        int B = sc.nextInt(); // end
        DFS(A);
        if(vis[B]) {
            System.out.println("Tangled");
        } else {
            System.out.println("Not Tangled");
        }
    }
}
