import java.util.*;

public class ccc10j5 {

    static class Pair {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
    }

    static int[][] dist;
    static boolean[][] vis;
    static Pair[] moves = {new Pair(1, 2), new Pair(1, -2), new Pair(2, 1), new Pair(2, -1), new Pair(-1, 2), new Pair(-1, -2), new Pair(-2, 1), new Pair(-2, -1)};
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int X1 = sc.nextInt();
        int Y1 = sc.nextInt();
        int X2 = sc.nextInt();
        int Y2 = sc.nextInt();
        dist = new int[10][10];
        vis = new boolean[10][10];
        Queue<Pair> q = new LinkedList<>();
        
        vis[X1][Y1] = true;
        dist[X1][Y1] = 0;
        q.add(new Pair(X1, Y1));

        while(!q.isEmpty()) {
            Pair cur = q.poll();
            int x = cur.f;
            int y = cur.s;
            for(Pair move : moves) {
                int nextX = x + move.f;
                int nextY = y + move.s;
                if(nextX > 0 && nextX <= 8 && nextY > 0 && nextY <= 8 && !vis[nextX][nextY]) {
                    q.add(new Pair(nextX, nextY));
                    vis[nextX][nextY] = true;
                    dist[nextX][nextY] = dist[x][y] + 1;
                }
            }
        }
        System.out.println(dist[X2][Y2]);
    }
}
