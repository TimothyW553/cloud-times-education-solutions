import java.util.*;

public class tree1 {
    
    static int[][] matrix;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        matrix = new int[4][4];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        int sum = 0;
        for(int i = 0; i < 4; i++) {
            int row = 0;
            for(int j = 0; j < 4; j++) {
                row += matrix[i][j];
                sum += matrix[i][j];
            }
            if(row == 0) { // the node is not connected to the tree
                System.out.println("No");
                return;
            }
        }
        if(sum == 6) { // V-1 edges
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
