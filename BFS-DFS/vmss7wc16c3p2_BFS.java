import java.util.*;

public class vmss7wc16c3p2_BFS {
    
    static ArrayList<Integer> adj[];
    static boolean[] vis;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int M = sc.nextInt();
        int A = sc.nextInt(); // start
        int B = sc.nextInt(); // end
        adj = new ArrayList[N+5];
        vis = new boolean[N+5];
        for(int i = 0; i < N+5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < M; i++) {
            int X = sc.nextInt();
            int Y = sc.nextInt();
            adj[X].add(Y); // bidirectional (X)-->(Y)
            adj[Y].add(X); //               (Y)-->(X)
        }
        Queue<Integer> q = new LinkedList<>();
        q.add(A);
        vis[A] = true;
        while(!q.isEmpty()) {
            int cur = q.poll();
            for(int v : adj[cur]) {
                if(!vis[v]) {
                    vis[v] = true;
                    q.add(v);
                }
            }
        }
        if(vis[B]) {
            System.out.println("GO SHAHIR!");
        } else {
            System.out.println("NO SHAHIR!");
        }
    }
}
