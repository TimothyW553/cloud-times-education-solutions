import java.util.*;
import java.io.*;

public class vmss7wc15c6p3 {

    static BufferedReader br;
    static PrintWriter out;
    static StringTokenizer st;

    static ArrayList<Integer> adj[];
    static int[] vals;

    static void DFS(int n) {
        for(int v : adj[n]) {
            DFS(v);
            vals[n] += vals[v];
        }
    }

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(new OutputStreamWriter(System.out));
        int N = readInt();
        adj = new ArrayList[N+5];
        vals = new int[N+5];
        for(int i = 0; i < N+5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < N-1; i++) {
            int a = readInt();
            int b = readInt();
            adj[a].add(b);
        }
        for(int i = 1; i <= N; i++) {
            vals[i] = readInt();
        }
        DFS(1);
        int best = Integer.MIN_VALUE;
        for(int i = 1; i <= N; i++) {
            best = Math.max(best, vals[i]);
        }
        System.out.println(best);
    }

    static String next () throws IOException {
        while (st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }

    static long readLong () throws IOException {
        return Long.parseLong(next());
    }

    static int readInt () throws IOException {
        return Integer.parseInt(next());
    }

    static double readDouble () throws IOException {
        return Double.parseDouble(next());
    }

    static char readCharacter () throws IOException {
        return next().charAt(0);
    }

    static String readLine () throws IOException {
        return br.readLine().trim();
    }
}
