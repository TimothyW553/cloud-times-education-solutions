import java.util.*;

public class ccc01s3 {

    static ArrayList<String> edges = new ArrayList<>();
    static ArrayList<Integer>[] graph = new ArrayList[30];
    static boolean[] vis = new boolean[30];

    static void DFS(int n) {
        vis[n] = true;
        for(int neighbour : graph[n]) {
            if(!vis[neighbour]) {
                DFS(neighbour);
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < 30; i++) {
            graph[i] = new ArrayList<>();
        }
        while(true) {
            String s = sc.next();
            if(s.equals("**")) {
                break;
            }
            edges.add(s);
        }
        int count = 0;
        for(int i = 0; i < edges.size(); i++) { // this for loop will determine which edge is going to be taken away
            for(int j = 0; j < 30; j++) {
                graph[j].clear();
            }
            Arrays.fill(vis, false);
            for(int j = 0; j < edges.size(); j++) {
                if(i != j) {
                    int x = edges.get(j).charAt(0)-'A';
                    int y = edges.get(j).charAt(1)-'A';
                    graph[x].add(y);
                    graph[y].add(x);
                }
            }
            DFS(0);
            if(!vis[1]) {
                count++;
                System.out.println(edges.get(i));
            }
        }
        System.out.println("There are " + count + " disconnecting roads.");
    }
}
