import java.util.*;

public class tsoc15c1p5 {
    
    static ArrayList<Integer> adj[];
    static boolean[] vis;
    static int[] distAnts;
    static int[] distBob;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int M = sc.nextInt();
        adj = new ArrayList[N+5];
        vis = new boolean[N+5];
        distAnts = new int[N+5];
        distBob = new int[N+5];
        for(int i = 0; i < N+5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < M; i++) {
            int X = sc.nextInt();
            int Y = sc.nextInt();
            adj[X].add(Y);
            adj[Y].add(X);
        }
        Queue<Integer> q = new LinkedList<>();
        int W = sc.nextInt();
        for(int i = 0; i < W; i++) {
            int X = sc.nextInt();
            q.add(X);
            vis[X] = true;
            distAnts[X] = 0;
        }
        while(!q.isEmpty()) {
            int cur = q.poll();
            for(int v : adj[cur]) {
                if(!vis[v]) {
                    vis[v] = true;
                    distAnts[v] = distAnts[cur] + 4;
                    q.add(v);
                }
            }
        }
        Arrays.fill(vis, false);
        distBob[1] = 0;
        vis[1] = true;
        q.add(1);
        while(!q.isEmpty()) {
            int cur = q.poll();
            for(int v : adj[cur]) {
                if(!vis[v] && distAnts[v] > distBob[cur] + 1) {
                    vis[v] = true;
                    distBob[v] = distBob[cur] + 1;
                    q.add(v);
                }
            }
        }
        if(vis[N]) {
            System.out.println(distBob[N]);
        } else {
            System.out.println("sacrifice bobhob314");
        }
    }
}
