import java.util.*;
import java.io.*;

public class ccc13s4 {

    static ArrayList<Integer>[] graph;
    static boolean[] vis; // vis[i] (T/F) if true, it means the node has been visited, otherwise it has not

    static void DFS(int n) {
        vis[n] = true;
        for(int neighbour : graph[n]) {
            if(!vis[neighbour]) {
                DFS(neighbour);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(new OutputStreamWriter(System.out));
        int N = readInt(); // number of nodes
        int M = readInt(); // number of connections
        graph = new ArrayList[N+5]; // + 5 so i dont get array index out of bounds
        vis = new boolean[N+5];
        for(int i = 0; i <= N; i++) { // initialize each of the ArrayLists
            graph[i] = new ArrayList<>();
        }
        for(int i = 0; i < M; i++) { // creating our graph
            int X = readInt();
            int Y = readInt();
            graph[X].add(Y); // creating the connection between node(X) and node(Y)
            // we dont put graph[Y].add(X) because it one directional
        }
        int P = readInt(); // src node
        int Q = readInt(); // dest node
        Arrays.fill(vis, false); // fill vis array with false because all nodes havent been vis
        DFS(P); // search from DFS(P)
        boolean pCanGoToq = vis[Q];
        Arrays.fill(vis, false);
        DFS(Q);
        boolean qCanGoTop = vis[P];
        if(pCanGoToq) {
            System.out.println("yes");
        } else if(qCanGoTop) {
            System.out.println("no");
        } else {
            System.out.println("unknown");
        }
    }

    static BufferedReader br;
    static PrintWriter out;
    static StringTokenizer st;

    static String next () throws IOException {
        while (st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }

    static long readLong () throws IOException {
        return Long.parseLong(next());
    }

    static int readInt () throws IOException {
        return Integer.parseInt(next());
    }

    static double readDouble () throws IOException {
        return Double.parseDouble(next());
    }

    static char readCharacter () throws IOException {
        return next().charAt(0);
    }

    static String readLine () throws IOException {
        return br.readLine().trim();
    }
}
