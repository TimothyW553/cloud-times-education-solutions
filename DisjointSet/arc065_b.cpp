#include <bits/stdc++.h>
using namespace std;

const int MAX = 2e5 + 5;

int parRoad[MAX], parRail[MAX];

map<pair<int, int>, int> freq; // {K: {road set, rail set}, V: frequency}

void make_set(int n) {
    parRoad[n] = n;
    parRail[n] = n;
}

int FindRoad(int n) {
    if(parRoad[n] == n) {
        return n;
    }
    return parRoad[n] = FindRoad(parRoad[n]);
}

int FindRail(int n) {
    if(parRail[n] == n) {
        return n;
    }
    return parRail[n] = FindRail(parRail[n]);
}

int main() {
    int n, k, l; cin >> n >> k >> l;
    for(int i = 1; i <= n; i++) {
        make_set(i);
    }
    for(int i = 0; i < k; i++) {
        int p, q; cin >> p >> q;
        p = FindRoad(p);
        q = FindRoad(q);
        if(p != q) {
            parRoad[p] = q;
        }
    }
    for(int i = 0; i < l; i++) {
        int r, s; cin >> r >> s;
        r = FindRail(r);
        s = FindRail(s);
        if(r != s) {
            parRail[r] = s;
        }
    }
    for(int i = 1; i <= n; i++) {
        FindRoad(i);
        FindRail(i);
    }
    for(int i = 1; i <= n; i++) {
        freq[{parRoad[i], parRail[i]}]++;
    }
    for(int i = 1; i <= n; i++) {
        cout << freq[{parRoad[i], parRail[i]}] << " ";
    }
    return 0;
}
