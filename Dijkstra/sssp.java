import java.util.*;

public class Main {

    static class Pair implements Comparable<Pair> {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
        public int compareTo(Pair other) {
            return s - other.s; // sort by ascending order
        }
    }

    static ArrayList<Pair> adj[];
    static PriorityQueue<Pair> pq;
    static int[] dist;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(); // number of nodes
        int M = sc.nextInt(); // number of edges
        adj = new ArrayList[N + 5];
        dist = new int[N + 5];
        pq = new PriorityQueue<>(); // {name of node, dist of node}
        for(int i = 0; i < N + 5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < M; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            adj[u].add(new Pair(v, w));
            adj[v].add(new Pair(u, w));
        }
        Arrays.fill(dist, 99999999);
        dist[1] = 0;
        pq.add(new Pair(1, dist[1]));
        while(!pq.isEmpty()) {
            Pair cur = pq.poll(); // extract the node with the smallest distance
            int u = cur.f;
            for(Pair e : adj[u]) {
                int v = e.f;
                int w = e.s;
                if(dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                    pq.add(new Pair(v, dist[v]));
                }
            }
        }
        for(int i = 1; i <= N; i++) {
            if(dist[i] != 99999999) { // has been visited
                System.out.println(dist[i]);
            } else {
                System.out.println(-1);
            }
        }
    }
}
