import java.util.*;
import java.io.*;

public class Main {

    static class Pair {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
    }
    
    static BufferedReader br;
    static PrintWriter out;
    static StringTokenizer st;

    static int[][] adj;
    static int[] dist;
    static boolean[] vis;
    static ArrayList<Pair> prices;
 
    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(new OutputStreamWriter(System.out));
        int N = readInt(); // # of cities
        int T = readInt(); // # of trade routes/edges

        adj = new int[N + 5][N + 5];
        dist = new int[N + 5];
        vis = new boolean[N + 5];
        prices = new ArrayList<>();

        for(int i = 0; i < T; i++) {
            int x = readInt();
            int y = readInt();
            int z = readInt();
            adj[x][y] = adj[y][x] = z;
        }
        
        int K = readInt();

        for(int i = 0; i < K; i++) {
            int z = readInt();
            int p = readInt();
            prices.add(new Pair(z, p));
        }

        int D = readInt();
        Arrays.fill(dist, 99999999);
        dist[D] = 0;

        for(int i = 1; i <= N; i++) {
            int smallest = 0;
            for(int j = 1; j <= N; j++) {
                if(!vis[j] && dist[j] < dist[smallest]) {
                    smallest = j;
                }
            }
            if(smallest == 0) {
                break;
            }
            vis[smallest] = true;
            for(int v = 1; v <= N; v++) {
                if(adj[smallest][v] > 0 && dist[smallest] + adj[smallest][v] < dist[v]) {
                    dist[v] = dist[smallest] + adj[smallest][v];
                }
            }
        }
        int mn = 99999999;
        for(Pair m : prices) {
            int price = dist[m.f] + m.s; // total price = dist + cost of pencil
            mn = Math.min(mn, price);
        }
        System.out.println(mn);
    }

    static String next () throws IOException {
        while (st == null || !st.hasMoreTokens())
        st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
 
    static long readLong () throws IOException {
        return Long.parseLong(next());
    }
 
    static int readInt () throws IOException {
        return Integer.parseInt(next());
    }
 
    static double readDouble () throws IOException {
        return Double.parseDouble(next());
    }
 
    static char readCharacter () throws IOException {
        return next().charAt(0);
    }
 
    static String readLine () throws IOException {
        return br.readLine().trim();
    }
}
