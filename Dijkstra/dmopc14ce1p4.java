import java.util.*;

public class Main {

    static class Pair implements Comparable<Pair> {
        int f;
        double s;
        public Pair(int f0, double s0) {
            f = f0; s = s0;
        }
        public int compareTo(Pair other) {
            return (int)(s - other.s);
        }
    }

    static ArrayList<Pair> adj[];
    static double[] dist;
    static PriorityQueue<Pair> pq;
    static int[] par;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int V = sc.nextInt();
        int E = sc.nextInt();

        adj  = new ArrayList[V + 5];
        dist = new double[V + 5];
        par  = new int[V + 5];
        pq   = new PriorityQueue<>();

        for(int i = 0; i < V + 5; i++) {
            adj[i] = new ArrayList<>();
        }

        for(int i = 0; i < E; i++) {
            int m = sc.nextInt();
            int n = sc.nextInt();
            double d = sc.nextDouble();
            double s = sc.nextDouble();
            adj[m].add(new Pair(n, d/s));
            adj[n].add(new Pair(m, d/s));
        }

        Arrays.fill(dist, 9999999);
        Arrays.fill(par, -1);

        dist[1] = 0;
        pq.add(new Pair(1, 0));
        while(!pq.isEmpty()) {
            Pair cur = pq.poll();
            int u = cur.f;
            for(Pair e : adj[u]) {
                int v = e.f;
                double w = e.s;
                double d = dist[u] + w;
                if(d < dist[v]) {
                    dist[v] = d;
                    pq.add(new Pair(v, dist[v]));
                    par[v] = u;
                }
            }
        }
        if(dist[V] == 9999999) {
            System.out.println(-1);
            return;
        }
        ArrayList<Integer> path = new ArrayList<>();
        for(int i = V; i != -1; i = par[i]) {
            path.add(i);
        }
        System.out.println(path.size() - 1);
        System.out.println(Math.round(dist[V]*60*1/3));
    }
}
