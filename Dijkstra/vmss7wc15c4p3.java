import java.util.*;

public class Main {

    static class Pair implements Comparable<Pair> {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
        public int compareTo(Pair other) {
            return s - other.s;
        }
    }

    static ArrayList<Pair> adj[];
    static int[] dist0;
    static int[] distn;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int M = sc.nextInt();

        adj = new ArrayList[N + 5];
        dist0 = new int[N + 5];
        distn = new int[N + 5];

        for(int i = 0; i < N + 5; i++) {
            adj[i] = new ArrayList<>();
        }

        for(int i = 0; i < M; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            adj[u].add(new Pair(v, w));
            adj[v].add(new Pair(u, w));
        }

        Arrays.fill(dist0, 9999999);
        Arrays.fill(distn, 9999999);

        PriorityQueue<Pair> q = new PriorityQueue<>();
        dist0[0] = 0;
        q.add(new Pair(0, 0)); // node, cost
        while(!q.isEmpty()) {
            int u = q.poll().f;
            for(Pair e : adj[u]) {
                int v = e.f;
                int w = e.s;
                int d = dist0[u] + w;
                if(d < dist0[v]) {
                    dist0[v] = d;
                    q.add(new Pair(v, d));
                }
            }
        }
        distn[N-1] = 0;
        q.add(new Pair(N-1, 0)); // node, cost
        while(!q.isEmpty()) {
            int u = q.poll().f;
            for(Pair e : adj[u]) {
                int v = e.f;
                int w = e.s;
                int d = distn[u] + w;
                if(d < distn[v]) {
                    distn[v] = d;
                    q.add(new Pair(v, d));
                }
            }
        }
        int mx = -1;
        for(int i = 0; i < N; i++) {
            mx = Math.max(mx, dist0[i] + distn[i]);
        }
        System.out.println(mx);
    }
}
