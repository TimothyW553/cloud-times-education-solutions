import java.util.*;

public class Main {

    static class Pair implements Comparable<Pair> {
        int f, s;
        public Pair(int f0, int s0) {
            f = f0; s = s0;
        }
        public int compareTo(Pair other) {
            return f-other.f;
        }
    }

    static int[] dist;
    static boolean[] vis;
    static int N, M;
    static ArrayList<Pair> adj[];

    static int DijkstraOptimized(int start, int end) {
        Arrays.fill(dist, 0x3f3f3f3f); // fill our distance array with infinity

        PriorityQueue<Pair> pq = new PriorityQueue<>(); // create a PriorityQueue of Pair {cost of edge, name of node}
        dist[start] = 0; // mark the distance of our source node, as 0
        q.add(new Pair(dist[start], start)); // put the source node into our PriorityQueue

        while(!pq.isEmpty()) { // process of all nodes
            Pair x = q.poll(); // take out node with smallest distance to the source node
            int u = x.s;       // this is achieved by doing q.poll() b/c we are using a PriorityQueue
            for(Pair e : adj[u]) { // go through all its neighbours
                int v = e.f;       // extract the name of the neighbour
                int w = e.s;       // extract the weight/cost of the edge
                if(dist[u] + w < dist[v]) { // RELAXATION FORMULA: dist[v] = dist[u] + w
                    dist[v] = dist[u] + w;  // update the distance of node v
                    pq.add(new Pair(dist[v], v)); // push back into our pqueue
                }
            }
        }
        return dist[end]; // distance to ending node
    }

    static int DijkstraClassic(int start, int end) {
        Arrays.fill(vis, false);
        Arrays.fill(dist, 0x3f3f3f3f);

        dist[start] = 0;
        for(int i = 1; i <= N; i++) {
            int u = 0; // looking for unvisited least dist node
            for(int j = 1; j <= N; i++) {
                if(!vis[j] && dist[j] < dist[u]) {
                    u = j;
                }
            }
            vis[u] = true;
            for(int v = 1; v <= N; v++) {
                int w = adj[u];
                if(!vis[v] && dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                }
            }
        }
        return dist[end];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();
        adj = new ArrayList[N+5];
        for(int i = 0; i < N+5; i++) {
            adj[i] = new ArrayList<>();
        }
        for(int i = 0; i < M; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            adj[u].add(new Pair(v, w));
            adj[v].add(new Pair(u, w));
        }
    }
}
