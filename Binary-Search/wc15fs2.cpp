#include <bits/stdc++.h>
using namespace std;
const int MN = 1e6 + 5;
int n, m, k;
int c[MN], t[MN];
bool works(int x) {
    bool flag = true;
    int currentCow = 0, currentTrough = 0, numCowsTroughJ = 0;
    while(1) {
        if(currentCow >= n) {
            break;
        }
        if(currentCow < n and currentTrough >= m) {
            flag = false;
            break;
        }
        if(numCowsTroughJ < x and t[currentTrough] <= c[currentCow] and t[currentTrough] >= c[currentCow] - k) {
            numCowsTroughJ++;
            currentCow++;
        } else {
            currentTrough++;
            numCowsTroughJ = 0;
        }
    }
    return flag;
}
int main() {
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    cin >> n >> m >> k;
    for(int i = 0; i < n; i++) {
        cin >> c[i];
    }
    for(int i = 0; i < m; i++) {
        cin >> t[i];
    }
    sort(c, c+n);
    sort(t, t+m);
    // binary searching on the time it takes for all cows to finish drinking from the troughs
    // limit the amount of cows at a trough, and check whether or not it works
    int lo = 1, hi = n + 1;
    while(lo < hi) {
        int mid = (lo + hi) / 2;
        if(works(mid)) {
            hi = mid;
        } else {
            lo = mid + 1;
        }
    }
    if(lo == n + 1) {
        cout << -1 << "\n";
    } else {
        cout << lo << "\n";
    }
    return 0;
}
