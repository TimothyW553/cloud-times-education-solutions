import java.util.*;
import java.io.*;

public class Main {

    static long[] fiveToThe;

    static long numOfTrailingZeros(long x) {
        long sum = 0;
        for(int i = 1;; i++) {
            if(fiveToThe[i] > x) {
                break;
            }
            sum += (x / fiveToThe[i]);
        }
        return sum;
    }

    static long lowerbound(long x) {
        long lo = 1, hi = fiveToThe[25];
        while(lo < hi) {
            long mid = (lo + hi) / 2;
            if(numOfTrailingZeros(mid) >= x) {
                hi = mid;
            } else {
                lo = mid + 1;
            }
        }
        return lo;
    }

    static long upperbound(long x) {
        long lo = 1, hi = fiveToThe[25];
        while(lo < hi) {
            long mid = (lo + hi) / 2;
            if(numOfTrailingZeros(mid) > x) {
                hi = mid;
            } else {
                lo = mid + 1;
            }
        }
        return lo;
    }

    public static void main(String[] args) {
        fiveToThe = new long[30]; // precomputing powers of 5
        fiveToThe[0] = 1;
        for(int i = 1; i <= 25; i++) {
            fiveToThe[i] = fiveToThe[i-1] * 5L;
        }
        long a = readLong();
        long b = readLong();
        long ita = lowerbound(a); // what is the first x! with a trailing zeros
        long itb = upperbound(b); // what is the first x! with b+1 trailing zeros
        System.out.println(itb - ita);
    }

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;

    static String read() {
        while (st == null || !st.hasMoreTokens()) {
            try { st = new StringTokenizer(br.readLine()); 
            } catch (IOException e) {}
        }
        return st.nextToken();
    }
    static long readLong() {
        return Long.parseLong(read());
    }
}
