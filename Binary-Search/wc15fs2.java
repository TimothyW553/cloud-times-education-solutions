import java.util.*;
import java.io.*;

public class Main {

	static int[] c, t; // height of cow[i], height of trough[i]
	static int N, M, K;

	static boolean valid(int x) {
		// valid(x) represents whether we can successfully filly one of the troughs with a
		// maximum of x cows. Notice that the max # of cows in 1 trough is the amount of time
		boolean flag = true;
		int i = 0, j = 0, cnt = 0; // i is current cow, j is current trough, cnt is number of cows at trough j
		while(true) {
			if(i >= N) { // finished prcocessing all cows and able to give trough to all
				break;
			}
			if(i < N && j >= M) { // cows left to be processed but not enough troughs
				flag = false;
				break;
			}
			if(cnt < x && t[j] <= c[i] && t[j] >= c[i] - K) { // if the number of cows in the current trough hasnt reach x yet and satisfies the conditions
				cnt++; // increase # of cows at current trough j
				i++; // process the next cow
			} else {
				j++; // go to next trough
				cnt = 0; // reset # of cows at current trough j
			}
		}
		return flag;
	}
	
	public static void main(String[] args) {
		N = readInt();
		M = readInt();
		K = readInt();
		c = new int[N];
		t = new int[M];
		for(int i = 0; i < N; i++) {
			c[i] = readInt();
		}
		for(int i = 0; i < M; i++) {
			t[i] = readInt();
		}
		Arrays.sort(c);
		Arrays.sort(t);
		int lo = 1, hi = N + 1;
		while(lo < hi) {
			int mid = (lo + hi) / 2;
			if(valid(mid)) {
				hi = mid;
			} else {
				lo = mid + 1;
			}
		}
		if(lo == N + 1) {
			System.out.println(-1);
		} else {
			System.out.println(lo);
		}
	}
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer st;

	static String read() {
		while (st == null || !st.hasMoreTokens()) {
			try { st = new StringTokenizer(br.readLine()); 
			} catch (IOException e) {}
		}
		return st.nextToken();
	}
	static int readInt() {
		return Integer.parseInt(read());
	}
	static long readLong() {
		return Long.parseLong(read());
	}
	static double readDouble() {
		return Double.parseDouble(read());
	}
}
