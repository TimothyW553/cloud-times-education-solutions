import java.util.*;
import java.io.*;

public class Main {

	static int N, K;
	static long[] arr;

	static boolean works(long x) {
		int groups = 0;
		long sum = 0; // sum for each group
		for(int i = 0; i < N; i++) {
			if(arr[i] > x) {
				return false;
			}
			if(sum + arr[i] > x) {
				groups++;
				sum = 0;
			}
			sum += arr[i];
		}
		if(sum > 0) {
			groups++;
		}
		return groups <= K; // groups > K, then the max sum, x, is too small
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		K = sc.nextInt();
		arr = new long[N];
		for(int i = 0; i < N; i++) {
			arr[i] = sc.nextLong();
		}
		// binary search on maximum sum in each subarray
		long lo = 0, hi = (long)1e18;
		while(lo < hi) {
			long mid = (lo + hi) / 2;
			if(works(mid)) {
				hi = mid;
			} else {
				lo = mid + 1;
			}
		}
		System.out.println(lo);
	}
}
/*
Assume you have a max sum: x, then you can divide the array into 
some number of s subarrays such that every subarray has sum < x
if we increase the the max sum, x, then the number of subarrays
either stays the same or decreases

	n = 4
	{10, 11, 12, 13}
	x: 27
	[10, 11], [12, 13]
	x: 28
	[10, 11], [12, 33]
	only changes when x: 33
*/
