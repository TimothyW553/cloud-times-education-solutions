import java.util.*;
import java.io.*;

public class Main {

	static int diff(int n1, int n2) { // calculates the distance between house n1 and house n2
		if(n1 > n2) {
			int tmp = n1;
			n1 = n2;
			n2 = tmp;
		}
		return Math.min(n2 - n1, n1 - n2 + 1000000);
		// since we can go either way around the cirle, we take the min
		// between going clockwise and counter-clockwise
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int H = sc.nextInt();
		int[] a = new int[H];
		for(int i = 0; i < H; i++) {
			a[i] = sc.nextInt();
		}
		Arrays.sort(a); // sorting to make the calculations more efficient
		int K = sc.nextInt();
		int lo = 0, hi = 500000; // binary search on the max fire hose length
		while(lo < hi) {
			int mid = (lo + hi) / 2;
			// limiting the maximum hose length to be at most mid
			int best = Integer.MAX_VALUE; // best is minimum number of fire hydrants needed to be placed if the limit is mid
			for(int i = 0; i < H; i++) {
				int cnt = 1, pos = a[i]; // # of fire hydrants placed, pos = pivot house
				for(int j = 1; j < H; j++) {
					if((diff(pos, a[(i+j)%H]) + 1) / 2 > mid) {
						pos = a[(i+j)%H];
						cnt++;
					}
				}
				best = Math.min(best, cnt);
			}
			if(best <= K) {
				hi = mid;
			} else {
				lo = mid + 1;
			}
		}
		System.out.println(lo);
	}
}
